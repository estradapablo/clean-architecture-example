
import express from 'express';
import { createUser, listUser } from '../../../controllers/users';
var routerUsers = express.Router();
/** RUTES **/
routerUsers.post('/create', async function (req, res, next) {
  /** Try Request **/
  try {
    /** Enviamos pedido al controlador**/
    const createNewUser = await createUser(req);

    /** Enviamos respuesta al delivery**/
    res.status(200).send({ ...createNewUser });

  } catch (err) {
    /** Catch request error **/
    next(err);
  }
});


routerUsers.post('/', async function (req, res, next) {
  /** Try Request **/
  try {
    /** Enviamos pedido al controlador**/
    const createNewUser = await listUser(req);

    /** Enviamos respuesta al delivery**/
    res.status(200).send({ ...createNewUser });

  } catch (err) {
    /** Catch request error **/
    next(err);
  }
});

export default routerUsers;
