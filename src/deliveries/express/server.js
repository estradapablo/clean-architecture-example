import express from 'express'
import bodyParser from 'body-parser'
import config from 'config';
import routerUsers from './routes/users.routes.js';

const app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());

/** Definimios metodo principal y rutas**/
app.use('/users', routerUsers);

/** Chequeo de Status **/
app.get('/healthcheck', function (req, res, next) {
  try {
    res.send({ status: 'healthy', uptime: process.uptime() });
  } catch (err) {
    next(err);
  }
});

app.use(function handleServerError(err, req, res, next) {
  return res.status(400).send({
    type: 'ServerError',
    message: err.message,
  });
});

app.listen(config.publicPort, function () {
  console.log(`EXPRESS Server running on ${config.publicPort}`);
});
