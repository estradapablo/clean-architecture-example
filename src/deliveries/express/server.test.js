import 'mocha';
import { expect } from 'chai';
import supertest from 'supertest';
import makeFakeUser from '../../../__test__/makeFakeUser';

const config = require('config');
const request = supertest(`http://localhost:${config.publicPort}`);

describe('Api EndPoints Test', () => {

  before(function () {
    /** Iniciamos el servidor **/
    require('./server');
  });

  it('POST /users/create | STATUS 200', async () => {

    const newUser = makeFakeUser();
    /** Mandamos el request a la API **/
    const response = await request
      .post('/users/create')
      .send(newUser)
      .set('Content-Type', 'application/json');

    const { status, body } = response;

    /** Evaluamos la respuesta **/
    expect(status).equal(200);
    expect(body).to.have.all.keys(
      'id',
      'name',
      'surname',
      'email',
      'birthday',
      'telephone',
      'reviews',
      'updateAt',
      'createdAt',
    );

  });


  it('POST /users/ | STATUS 200', async () => {

    /** Mandamos el request a la API **/
    const response = await request
      .post('/users/')
      .send({})
      .set('Content-Type', 'application/json');

    const { status, body } = response;
    /** Evaluamos la respuesta **/
    expect(status).equal(200);


  });

});
