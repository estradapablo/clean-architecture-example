export default function exampleIntegration({ apiExample }) {
  return Object.freeze({
    findReviews,
  });

  async function findReviews({ ...params }) {

    return await apiExample(params);

  }

}