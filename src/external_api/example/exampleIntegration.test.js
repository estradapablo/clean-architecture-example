import 'mocha';
import * as chai from 'chai';
import chaiAsPromised from 'chai-as-promised';
import exampleApi from './';
import makeFakeUser from '../../../__test__/makeFakeUser';

const expect = chai.expect
chai.use(chaiAsPromised)

const newUser = makeFakeUser();
describe('Example Integration Test', () => {

  it('Find Reviews', async () => {
    const result = await exampleApi.findReviews({});
    expect(result).to.be.an('array');

  })


});