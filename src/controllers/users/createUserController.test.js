import 'mocha';
import * as chai from 'chai';
import createUserController from './createUserController';
import { createUser } from './';
import makeFakeUser from '../../../__test__/makeFakeUser';
import chaiAsPromised from 'chai-as-promised';
const expect = chai.expect
chai.use(chaiAsPromised)


describe('Create User Controller', () => {
  it('Create User Controller Ok', async () => {

    const newUser = createUserController({
      createNewUser: (user) => {
        user.user.id = 1;
        return user
      }
    })

    const newFakeUser = makeFakeUser();
    const request = {
      body: newFakeUser,
    };
    const expected = {
      user: {
        ...newFakeUser,
        id: 1,
      }
    };
    const actual = await newUser(request);
    expect(actual).to.eql(expected);

  });

  it('Create User Controller Error', async () => {

    const newUser = createUserController({
      createNewUser: (user) => Promise.reject(new Error('Error in controller'))
    });

    const newFakeUser = makeFakeUser();
    const request = {
      body: newFakeUser,
    };

    await expect(newUser(request))
      .to.be.rejectedWith('Error in controller');

  })

  it('Create User Controller Index - Ok', async () => {
    const newFakeUser = makeFakeUser();
    const request = {
      body: newFakeUser,
    };
    const newUser = await createUser(request);

    expect(newUser).to.have.all.keys(
      'id',
      'name',
      'surname',
      'email',
      'birthday',
      'telephone',
      'reviews',
      'updateAt',
      'createdAt',
    );

  });

});