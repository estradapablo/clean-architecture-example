import 'mocha';
import * as chai from 'chai';
import listUserController from './listUserController';

import chaiAsPromised from 'chai-as-promised';
const expect = chai.expect
chai.use(chaiAsPromised)


describe('list User Controller', () => {
  it('list User Controller Ok', async () => {

    const listUser = listUserController({
      listUsers: () => {
        return [{}, {}, {}]
      }
    })

    const listUsers = await listUser({});
    expect(listUsers.length).to.eql(3);

  });
});