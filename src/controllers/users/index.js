/** Importamos los casos de uso para inyectarols en los controladores **/
import { createNewUser, listUsers } from '../../usecases/users';

/** Importamos los controladores **/
import createUserController from './createUserController';
import listUserController from './listUserController'

/** Construimos las funciones de los casos de uso inyectando las dependencias ej: { createNewUser }**/

const createUser = createUserController({ createNewUser });
const listUser = listUserController({ listUsers });

/** Lo transformamos en un objeto solo de lectura**/
const userController = Object.freeze({
  createUser,
  listUser
})

/** Exponemos el controlador y sus propiedades para que las usen los deliveries **/
export default userController;
export { createUser, listUser }