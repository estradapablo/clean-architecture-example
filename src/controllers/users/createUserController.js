/** El caso de uso es inyectado como dependencia en el primer () Ej:({ createNewUser }) **/
const createUserController = ({ createNewUser }) => async (req) => {

  const { name, surname, email, birthday, telephone } = req.body;

  /** Le mandamos a nuestro caso de uso solo la data que requiere **/
  return await createNewUser({ user: { name, surname, email, birthday, telephone } });

};

export default createUserController;