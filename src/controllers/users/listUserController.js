const listUserController = ({ listUsers }) => async (req) => {

  return await listUsers({ ...req.body });

};

export default listUserController;