import 'mocha';
import * as chai from 'chai';
import createUser from './createUser';
import { createNewUser } from './';
import makeFakeUser from '../../../__test__/makeFakeUser';
import makeFakeDb from '../../../__test__/makeFakeDb';
import exampleApi from '../../external_api/example'
import chaiAsPromised from 'chai-as-promised';
const expect = chai.expect
chai.use(chaiAsPromised)


describe('Create User - Use Case', () => {
  it('Create User Ok', async () => {
    const newUser = makeFakeUser();
    const user = await createUser({ exampleApi, userModel: makeFakeDb })({ user: newUser });

    expect(user).to.have.all.keys(
      'id',
      'name',
      'surname',
      'email',
      'birthday',
      'telephone',
      'reviews',
      'updateAt',
      'createdAt',
    );

  });

  it('Create User - User must have a name', async () => {

    const newUser = makeFakeUser({ name: null });
    await expect(createUser({ exampleApi, userModel: makeFakeDb })({ user: newUser }))
      .to.be.rejectedWith('User must have a name.');

  });

  it('Create User - User must have a surname', async () => {

    const newUser = makeFakeUser({ surname: null });
    await expect(createUser({ exampleApi, userModel: makeFakeDb })({ user: newUser }))
      .to.be.rejectedWith('User must have a surname.');

  });

  it('Create User - User must have a email', async () => {

    const newUser = makeFakeUser({ email: null });
    await expect(createUser({ exampleApi, userModel: makeFakeDb })({ user: newUser }))
      .to.be.rejectedWith('User must have a email.');

  });

  it('Create User - You have entered an invalid email address.', async () => {

    const newUser = makeFakeUser({ email: 'errormail.com' });
    await expect(createUser({ exampleApi, userModel: makeFakeDb })({ user: newUser }))
      .to.be.rejectedWith('You have entered an invalid email address.');

  });

  it('Create User - Telephone is not a number', async () => {

    const newUser = makeFakeUser({ telephone: 'not-number' });
    await expect(createUser({ exampleApi, userModel: makeFakeDb })({ user: newUser }))
      .to.be.rejectedWith('Telephone is not a number.');

  });

  it('Create User - User already exist', async () => {

    const newUser = makeFakeUser();
    const copyMakeFakeDb = Object.assign({}, makeFakeDb)
    copyMakeFakeDb.findOne = () => true;
    await expect(createUser({ exampleApi, userModel: copyMakeFakeDb })({ user: newUser }))
      .to.be.rejectedWith('User already exist!');

  });

  it('Create User - Save error', async () => {

    const newUser = makeFakeUser();
    const copyMakeFakeDb = Object.assign({}, makeFakeDb)
    copyMakeFakeDb.save = () => Promise.reject(new Error('Save error'));
    await expect(createUser({ exampleApi, userModel: copyMakeFakeDb })({ user: newUser }))
      .to.be.rejectedWith('Save error');

  });

  it('Create New User Index OK', async () => {

    const newUser = makeFakeUser();
    const user = await createNewUser({ user: newUser });

    expect(user).to.have.all.keys(
      'id',
      'name',
      'surname',
      'email',
      'birthday',
      'telephone',
      'reviews',
      'updateAt',
      'createdAt',
    );

  });

});