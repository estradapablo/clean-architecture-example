import createUser from './createUser';
import listUser from './listUser';
import instanceUserDb from '../../models/users';
import exampleApi from '../../external_api/example';

const createNewUser = createUser({ userModel: instanceUserDb, exampleApi });
const listUsers = listUser({ userModel: instanceUserDb })

const user = Object.freeze({
  createNewUser,
  listUsers
})

export { createNewUser, listUsers }