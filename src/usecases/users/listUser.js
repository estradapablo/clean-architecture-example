const createUser = ({ userModel }) => async ({ ...params }) => {

  const existUser = await userModel.find({ ...params });

  return existUser;
};

export default createUser;