import 'mocha';
import * as chai from 'chai';
import listUser from './listUser';
import { listUsers } from './';
import makeFakeDb from '../../../__test__/makeFakeDb';
import chaiAsPromised from 'chai-as-promised';
const expect = chai.expect
chai.use(chaiAsPromised)


describe('List User - Use Case', () => {
  it('List User Ok', async () => {

    const users = await listUser({ userModel: makeFakeDb })({});
    expect(users.length).to.eql(3);

  });

  it('List User = 0 Ok', async () => {

    const copyMakeFakeDb = Object.assign({}, makeFakeDb)
    copyMakeFakeDb.find = () => [];
    const users = await listUser({ userModel: copyMakeFakeDb })({});
    expect(users.length).to.eql(0);

  });

  it('List Users Index Ok', async () => {

    const users = await listUsers({});
    expect(users).to.be.an('array');

  });

});