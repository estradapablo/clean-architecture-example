import userEntitie from '../../entities/user';
const createUser = ({ userModel, exampleApi }) => async ({ user }) => {
  /** Create Entitie **/
  const newUser = await userEntitie(user);
  const { name, surname, email, birthday, telephone, updateAt, createdAt } = newUser;
  /** Find in databes if user exist **/
  const existUser = await userModel.findOne({ email });

  if (existUser) {
    throw new Error('User already exist!');
  }

  /** Example API CALL **/
  const reviews = await exampleApi.findReviews({});

  /** Save new user and return **/
  const saveNewUser = await userModel.save({
    name,
    surname,
    email,
    birthday,
    telephone,
    updateAt,
    createdAt,
  });

  return {
    id: saveNewUser.id,
    name: saveNewUser.name,
    surname: saveNewUser.surname,
    email: saveNewUser.email,
    birthday: saveNewUser.birthday,
    telephone: saveNewUser.telephone,
    reviews: reviews,
    updateAt: saveNewUser.updated_at,
    createdAt: saveNewUser.created_at,
  };
};

export default createUser;