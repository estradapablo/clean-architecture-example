export default function userModel({ userModel }) {
  return Object.freeze({
    find,
    findOne,
    save,
  });

  async function find({ ...params }) {
    if (params) {
      return await userModel('users').where(params);
    }
    return await userModel('users');
  }

  async function findOne({ ...user }) {
    return await userModel('users').where(user).first();
  }

  async function save({ name, surname, email, birthday, telephone, updateAt, createdAt, }) {
    const [result] = await userModel('users').insert({
      name,
      surname,
      email,
      birthday,
      telephone,
      updated_at: updateAt,
      created_at: createdAt,
    }).returning('*');
    return result
  }
}