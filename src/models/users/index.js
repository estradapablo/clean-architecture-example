import config from 'config';

import userModel from './userModel'
import knexBuilder from 'knex'

const instanceDb = knexBuilder(config);
const instanceUserDb = userModel({ userModel: instanceDb });

export default instanceUserDb;
