import 'mocha';
import * as chai from 'chai';
import chaiAsPromised from 'chai-as-promised';
import instanceUserDb from './';
import makeFakeUser from '../../../__test__/makeFakeUser';

const expect = chai.expect
chai.use(chaiAsPromised)

const newUser = makeFakeUser();
describe('User - Database', () => {

  it('Save a user Index', async () => {

    const result = await instanceUserDb.save(newUser)
    expect(result).to.have.all.keys(
      'id',
      'name',
      'surname',
      'email',
      'birthday',
      'telephone',
      'updated_at',
      'created_at',
    );

  })


  it('Find user Index', async () => {
    const result = await instanceUserDb.findOne({ email: newUser.email })
    expect(result).to.have.all.keys(
      'id',
      'name',
      'surname',
      'email',
      'birthday',
      'telephone',
      'updated_at',
      'created_at',
    );
  })


  it('Find users', async () => {
    const result = await instanceUserDb.find({});
    expect(result).to.be.an('array');

  })


});