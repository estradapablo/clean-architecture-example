import 'mocha';
import { expect } from 'chai';
import userEntitie from './'
import makeFakeUser from '../../../__test__/makeFakeUser';

describe('Users Entities', () => {
  it('Users Define Ok', async () => {
    const newUser = makeFakeUser();
    const user = userEntitie(newUser);
    expect(user).to.have.all.keys(
      'name',
      'surname',
      'email',
      'birthday',
      'telephone',
      'updateAt',
      'createdAt',
    );

    expect(user.name).to.equal(newUser.name);
    expect(user.surname).to.equal(newUser.surname);
    expect(user.email).to.equal(newUser.email);
    expect(user.birthday).to.equal(newUser.birthday);
    expect(user.telephone).to.equal(newUser.telephone);

  });

  it('Users Define - User must have a name', async () => {

    const newUser = makeFakeUser({ name: null });
    expect(() => userEntitie(newUser)).to.throw('User must have a name.');

  });

  it('Users Define - User must have a surname', async () => {

    const newUser = makeFakeUser({ surname: null });
    expect(() => userEntitie(newUser)).to.throw('User must have a surname.');

  });

  it('Users Define - User must have a email', async () => {

    const newUser = makeFakeUser({ email: null });
    expect(() => userEntitie(newUser)).to.throw('User must have a email.');

  });


  it('Users Define - You have entered an invalid email address.', async () => {

    const newUser = makeFakeUser({ email: 'errormail.com' });
    expect(() => userEntitie(newUser)).to.throw('You have entered an invalid email address.');

  });

  it('Users Define - Telephone is not a number', async () => {
    const newUser = makeFakeUser({ telephone: 'not-number' });
    expect(() => userEntitie(newUser)).to.throw('Telephone is not a number.');

  });

});