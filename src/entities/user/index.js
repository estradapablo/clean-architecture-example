import user from './user';
import moment from 'moment-timezone';
const dateFormat = (date = null) => {
  var now = new Date();
  const newDate = moment(date || now).tz('America/Argentina/Buenos_Aires').format('YYYY-MM-DD HH:mm:ss');
  return newDate;
};

const validateEmail = mail => {
  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
    return (true)
  }
  return (false)
}

const userEntitie = user({ dateFormat, validateEmail })

export default userEntitie;

