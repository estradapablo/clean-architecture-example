const user = ({ dateFormat, validateEmail }) => ({
  name,
  surname,
  email,
  birthday,
  telephone,
  updateAt = dateFormat(),
  createdAt = dateFormat(),
}) => {

  if (!name) {
    throw new Error('User must have a name.')
  }

  if (!surname) {
    throw new Error('User must have a surname.')
  }

  if (!email) {
    throw new Error('User must have a email.')
  }

  if (email && !validateEmail(email)) {
    throw new Error('You have entered an invalid email address.')
  }

  if (telephone && isNaN(telephone)) {
    throw new Error('Telephone is not a number.')
  }


  return Object.freeze({
    name,
    surname,
    email,
    birthday,
    telephone,
    updateAt,
    createdAt,
  })
}


export default user;
