# Instalar dependencias

```
yarn
```

```
npm
```

# Ejemplo para crear migraciones con Knex

### Configurar la base de datos en el archivo .env

DB_USER=postgres
DB_PASSWORD=password
DB_NAME=database_name
DB_PORT=5432
DB_HOST=127.0.0.1

### Instalar knex global para la CLI

```
npm install knex -g
```

### Correr migraciones

Crear la base de datos previo a correr la migracion y despues:

```
npm migrate:up
```

```
yarn migrate:up
```

# Iniciar Servidor express

```
yarn start
```

# Ejemplos Test unitarios y end to end

```
yarn test
```

Create User Controller
✓ Create User Controller Ok
✓ Create User Controller Error
✓ Create User Controller Index - Ok (48ms)

list User Controller
✓ list User Controller Ok

Api EndPoints Test
EXPRESS Server running on 8080
✓ POST /users/create | STATUS 200 (53ms)
✓ POST /users/ | STATUS 200

Users Entities
✓ Users Define Ok
✓ Users Define - User must have a name
✓ Users Define - User must have a surname
✓ Users Define - User must have a email
✓ Users Define - You have entered an invalid email address.
✓ Users Define - Telephone is not a number

Example Integration Test
✓ Find Reviews

User - Database
✓ Save a user Index
✓ Find user Index
✓ Find users

Create User - Use Case
✓ Create User Ok
✓ Create User - User must have a name
✓ Create User - User must have a surname
✓ Create User - User must have a email
✓ Create User - You have entered an invalid email address.
✓ Create User - Telephone is not a number
✓ Create User - User already exist
✓ Create User - Save error
✓ Create New User Index OK

List User - Use Case
✓ List User Ok
✓ List User = 0 Ok
✓ List Users Index Ok
