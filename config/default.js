const dotenv = require('dotenv');
dotenv.config();

module.exports = {
  name: process.env.NAME || 'Aplication Name',
  publicPort: process.env.PUBLIC_PORT || 8080,
  client: 'postgres',
  connection: {
    host: process.env.DB_HOST || 'localhost',
    user: process.env.DB_USER || 'postgres',
    password: process.env.DB_PASSWORD || 'password',
    database: process.env.DB_NAME || 'database_name',
    port: process.env.DB_PORT || 5432,
  },
  migrations: {
    directory: '../migrations'
  },
  debug: false
};
