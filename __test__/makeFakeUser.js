import faker from 'faker';

const makeFakeUser = (user) => {

  const userFake = {
    name: faker.name.firstName(),
    surname: faker.name.lastName(),
    email: faker.internet.email(),
    birthday: '2001-01-01',
    telephone: '1111111111',
  };

  return {
    ...userFake,
    ...user,
  };

};

export default makeFakeUser;
