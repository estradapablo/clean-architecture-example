const makeFakeDb = function () {
  return Object.freeze({
    find,
    findOne,
    save,
  });

  function findOne({ user } = {}) {
    return false;
  }

  function find() {
    return [{}, {}, {}];
  }

  function save({
    name,
    surname,
    email,
    birthday,
    telephone,
    updateAt,
    createdAt,
  }) {
    return {
      id: 1,
      name,
      surname,
      email,
      birthday,
      telephone,
      updateAt,
      createdAt,
    };
  }
}

export default makeFakeDb();
